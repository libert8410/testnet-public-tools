# How to install and setup MetaMask

## Installing the Extension

The first thing you need to do is install the add-on MetaMask in your browser. To do this, visit the [Metamask Download Page](https://metamask.io/download.html), which offers the Metamask extension for the browsers Chrome, Brave, Firefox, Edge and as an application for Android and iOS phones. This tutorial covers the steps for the Chrome extension, however for other browsers, the steps are nearly identical.

## Account Creation

After installing the extension, you will be presented with a page for connecting an existing account or creating a new one.
Select *create an account*, agree to the proposed agreements, and create your password to access your account. You will be prompted to view the seed phrase. Please store it in a safe way, as it will be required for you to back up and restore your account.

## Network Configuration

You need to configure your newly created wallet to support and accept Q tokens. To do this, you need to connect to the Q blockchain. On the main page of your account, select *Custom RPC* in the upper right corner in the list of networks. Then enter the following details to setup the connection to Q:

| **Parameter** | **Value** |
|:--|:--|
| Network name | Q Testnet|
| RPC-URL | https://rpc.qtestnet.org |
| Chain-ID | 35443 |
| Symbol | Q |
| Block-Explorer-URL| https://explorer.qtestnet.org |

> **Note: ** *These parameters are valid for Q testnet only!*

## Faucet

Q testnet offers a token faucet that gives away free tokens of all supported types. These free tokens are only valid on Q testnet.

Go to the faucet UI at [`https://faucet.qtestnet.org`](https://faucet.qtestnet.org), enter your wallet address, select the token you want to receive (Q, QUSD or QBTC) and click on "claim tokens".

If the faucet is funded enough, your balance will increase within a couple of seconds. If the faucet has run out of funds, drop us a note on our [Discord](https://discord.gg/YTgkvJvZGD) server.
